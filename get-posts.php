<?php
/**
 * Plugin Name:     Get Posts
 * Description:     A block to embed CPT inside content
 * Version:			1.0.1
 * Author:          Fantassin
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     fantassin
 *
 * @package         fantassin
 */

/**
 * Registers all block assets so that they can be enqueued through the block editor
 * in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/applying-styles-with-stylesheets/
 */
function fantassin_get_post_block_init() {
	$dir = dirname( __FILE__ );

	$script_asset_path = "$dir/build/index.asset.php";
	if ( ! file_exists( $script_asset_path ) ) {
		throw new Error(
			'You need to run `npm start` or `npm run build` for the "create-block/get-post" block first.'
		);
	}
	$index_js     = 'build/index.js';
	$script_asset = require( $script_asset_path );

	wp_register_script(
		'fantassin-get-post-block-editor',
		plugins_url( $index_js, __FILE__ ),
		$script_asset['dependencies'],
		$script_asset['version']
	);

	wp_set_script_translations( 'create-block-get-post-block-editor', 'get-post' );

	register_block_type( 'fantassin/get-post', array(
		'editor_script' => 'fantassin-get-post-block-editor',
	) );
}
add_action( 'init', 'fantassin_get_post_block_init' );
