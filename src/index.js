/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

/**
 * Internal dependencies
 */
import edit from './edit';
import attributes from './attributes';
import save from './save';

registerBlockType( 'fantassin/post', {
	title: __( 'Post', 'fantassin' ),
	description: __( 'Include post inside your content', 'fantassin' ),
	keywords: [ __( 'post', 'fantassin' ), __( 'link', 'fantassin' ) ],
	supports: {
		anchor: true,
		html: false,
	},
	category: 'common',
	icon: 'welcome-add-page',
	attributes,
	edit,
	save,
} );
