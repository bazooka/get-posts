import { __ } from '@wordpress/i18n';
import { debounce, uniqBy } from 'lodash';
import { useEffect, useState } from '@wordpress/element';
import apiFetch from '@wordpress/api-fetch';
import { FormTokenField, Placeholder } from '@wordpress/components';
import { addQueryArgs } from '@wordpress/url';
import { useSelect } from '@wordpress/data';

const PostEdit = ( { setAttributes, attributes } ) => {
	const { posts: postsSelected, postType } = attributes;

	const [ postsSuggestion, setPostsSuggestion ] = useState( [] );

	const options = {
		per_page: -1,
		post_status: 'publish',
	};

	const fetchPostsSuggestion = async ( search = '' ) => {
		if ( ! postType ) {
			return;
		}

		const items = await apiFetch( {
			path: addQueryArgs( `/wp/v2/${ postType }s`, {
				...options,
				search: search,
			} ),
		} );

		setPostsSuggestion( uniqBy( [ ...postsSuggestion, ...items ], 'id' ) );
	};

	const handleSelectChange = ( e ) => {
		setAttributes( { postType: e.target.value.id } );
	};

	const handleDebounceInputChange = debounce(
		async ( searchValue ) => {
			fetchPostsSuggestion( searchValue );
		},
		300,
		{ leading: false, trailing: true }
	);

	const handleFormTokenFieldInputChange = ( data ) =>
		handleDebounceInputChange( data );

	const handleFormTokenFieldChange = ( tokens ) => {
		const posts = tokens
			.filter( ( value ) =>
				postsSuggestion.find( ( post ) => post.title.rendered === value )
			)
			.map( ( value ) =>
				postsSuggestion.find( ( post ) => post.title.rendered === value )
			);

		console.log(posts);
		setAttributes( { posts } );
	};

	const postTypes = useSelect( ( select ) => {
		const data = select( 'core' ).getPostTypes() || [];
		return data.filter( ( item ) => {
			if ( ! item.viewable || item.slug !== 'post' ) {
				return false;
			}

			return true;
		} );
	}, [] );

	useEffect( () => {
		fetchPostsSuggestion();
	}, [ postType ] );

	return (
		<Placeholder
			icon="welcome-add-page"
			instructions={ __( 'Search and include posts' ) }
			label={ __( 'Posts' ) }
		>
			<div>
				{ postTypes.length > 0 && (
					<select value={ postType } onChange={ handleSelectChange }>
						{ postTypes.map( ( item ) => (
							<option key={ item.slug } value={ item.slug }>
								{ item.name }
							</option>
						) ) }
					</select>
				) }
				<FormTokenField
					value={
						postsSelected &&
						postsSelected.map( ( item ) => item.title.rendered )
					}
					suggestions={ postsSuggestion.map(
						( item ) => item.title.rendered
					) }
					onInputChange={ handleFormTokenFieldInputChange }
					onChange={ handleFormTokenFieldChange }
				/>
			</div>
		</Placeholder>
	);
};

export default PostEdit;
