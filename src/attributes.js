export default {
	posts: {
		type: 'array',
		default: [],
	},
	postType: {
		type: 'string',
		default: 'post',
	},
};
